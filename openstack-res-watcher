#!/bin/bash

 

######################################################################################
#
# GPLv2
#
# openstack-res-watcher
# Copyright (C) 2019 Martin Bochnig opensxce-lx64.org mb1x@gmx.com
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
# USA.
#
# Usage: openstack-res-watcher ...
#
# Prints a summary concerning which individual OpenStack
# user in the cluster consumes how many infrastructure resources 
# (VCPU's, RAM, Ephemeral disks, Volumes) - on a per openstack-user basis.
#
#
# Options:
#  -h --help    ... help message
#
#
# Limitations: 
#
#  nothing more than a simplistic quick hack, rather than a real backend solution
#
#  e.h.: Among much else, no instances with status ERROR are handled yet, no support
#
#        for Snapshots, let alone Object storage ...
#
#
######################################################################################

 
die() { echo "$1"; exit 1; }

usage() {
cat <<EOF

Usage: getargs [--help] [OPTION...] ARGUMENT...

"Prints a summary concerning which individual OpenStack 
 user in the cluster consumes how many infrastructure resources
 (VCPU's, RAM, Ephemeral disks, Volumes) - on a per openstack-user basis."

Options:
  -h, --help            display this help message

EOF
}

# Option parsing
while [ $# -gt 0 ]; do
    case "$1" in
        --*=*)      a="${1%%=*}"; b="${1#*=}"; shift; set -- "$a" "$b" "$@" ;;
        -h|--help)  usage; exit 0; shift ;;
        --)         shift; break ;;
        -*)         usage; die "Invalid option: $1" ;;
        *)          break ;;
    esac
done


##### FUNCTION DEFINITIONS *must* come 1st in bash

function print_delimiter()
{
echo "######################################################################################"
echo "######################################################################################"
}

function user_list()
{
openstack user list -f yaml -c Name|cut -d " " -f 3
}

function server_list()
{
openstack server list --all --user $1 -c ID -f yaml|cut -d " " -f 3
}

function server_show()
{
openstack server show -f shell -c user_id -c key_name -c flavor -c volumes_attached $1 
}

function server_show_flavor()
{
openstack server show -f yaml -c flavor $1|cut -d " " -f 2
}

function flavor_show()
{
openstack flavor show -f shell -c vcpus -c ram -c disk $1
}

function volumes_attached()
{
volumes_attached_result=$(openstack server show -c volumes_attached -f shell $1|cut -d "'" -f 2) 

   if [ -z $volumes_attached_result ]

   then

     echo "No volume attached."

   else

     for vol in $volumes_attached_result
       do volumes_show $vol
     done
   fi
   unset volumes_attached_result
}

function volumes_show()
{
openstack volume show -f shell -c size $1
}
   


##### ACTUAL WORK

timestamp=$(date +""%Y%m%d-%H%M%S"")
mkdir $timestamp && cd $timestamp
user_list_results=$(user_list)
for i in $user_list_results

  do server_list_results=$(server_list $i)
    print_delimiter
    if [ "$server_list_results" != '[]' ]
      then
        for ii in $server_list_results

          do server_show $ii
            flavor=$(server_show_flavor $ii)
            flavor_show $flavor

            volumes_attached $ii

            print_delimiter

        done 2> /dev/null|tee -a $i-total.txt ###user$ioutput$timestamp.txt

        print_delimiter && print_delimiter
resources="vcpus ram disk volumes"
for res in $resources
        do
        if [ $res != volumes ]
        then
            ures=$(grep $res $i-total.txt|cut -d "\"" -f 2)
        else
            ures=$(grep size $i-total.txt|cut -d "\"" -f 2)
        fi

        r=0

        for iii in $ures
          do let r=$r+$iii
        done
        if [ $res == ram ]
        then
            echo "Total $res consumed by user $i: $r MB" 2> /dev/null|tee -a total-$res.txt
        elif [ $res == vcpus ]
        then
            echo "Total $res consumed by user $i: $r vCores" 2> /dev/null|tee -a total-$res.txt
        else
            echo "Total $res consumed by user $i: $r GB" 2> /dev/null|tee -a total-$res.txt
        fi
done
        print_delimiter && print_delimiter


    else
      echo "User $i currently has no VM running here, nothing to show ..."
    fi

done 2> /dev/null|tee -a total_output$timestamp.txt

print_delimiter && print_delimiter
resources="vcpus ram disk volumes"
for res in $resources
        do
        if [ $res != volumes ]
        then
            ures=$(grep $res *-total.txt|cut -d "\"" -f 2)
        else
            ures=$(grep size *-total.txt|cut -d "\"" -f 2)
        fi

        r=0

        for iii in $ures
          do let r=$r+$iii
        done 
        if [ $res == ram ]
        then
            echo "Total $res consumed by all users: $r MB" 2> /dev/null|tee -a total-$res.txt
        elif [ $res == vcpus ]
        then
            echo "Total $res consumed by all users: $r vCores" 2> /dev/null|tee -a total-$res.txt        
        else
            echo "Total $res consumed by all users: $r GB" 2> /dev/null|tee -a total-$res.txt        
        fi
done 2> /dev/null|tee -a total_output$timestamp.txt
print_delimiter && print_delimiter

