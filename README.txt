######################################################################################
#
# GPLv2
#
# openstack-res-watcher
# Copyright (C) 2019 Martin Bochnig opensxce-lx64.org mb1x@gmx.com
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
# USA.
#
# Usage: openstack-res-watcher ...
#
# Prints a summary concerning which individual OpenStack
# user in the cluster consumes how many infrastructure resources
# (VCPU's, RAM, Ephemeral disks, Volumes) - on a per openstack-user basis.
#
#
# Options:
#  -h --help    ... help message
#
#
# Limitations:
#
#  nothing more than a simplistic quick hack, rather than a real backend solution
#
#  e.h.: Among much else, no instances with status ERROR are handled yet, no support
#
#        for Snapshots, let alone Object storage ...
#
#
######################################################################################

